INTRODUCTION
------------
Drupal integration for phpamo, a library to o create urls for Camo - the
SSL image proxy.

CONTEXT
-------
Camo is a tool created by GitHub that provides a proxy server implemented in
node.js to solve mixed-content problems in your HTTPS page due to insecure
images.

The problem arises when your site allows users to add external images (often
through Wysiwyg fields) in content. The use of external insecure sources in
pages using a secure connection (HTTPS) will cause a mixed-content warning in
your browser.

MODULE
------
This module integrates the PHP library phpamo, implemented to convert insecure
image sources to secure URLs that can be understood by your Camo server, with
your Drupal site, providing a text filter for performing the process.

REQUIREMENTS
------------

This module requires the following modules:

 - Libraries - 2.X (https://drupal.org/project/libraries)
 - Library phpamo (https://github.com/willwashburn/phpamo)

INSTALLATION
------------

 - Install the module "Libraries" (2.X):
   https://www.drupal.org/project/libraries.

 - Download the library code (naming it "phpamo") and locate it in your
   libraries folder (by default - sites/all/libraries):

   * Download and extract the zip version from
     https://github.com/willwashburn/phpamo/archive/master.zip ... or
   * Use GIT to clone the project:
     git clone https://github.com/willwashburn/phpamo.git

 - The folder structure of the library must look like:

   phpamo
      |------ composer.json
      |------ license.txt
      |------ phpunit.xml
      |------ README.md
      |------ src
               |------ Encoder
                          |------ ...
               |------ Formatter
                          |------ ...
               |------ Phpamo.php
      |------ tests
               |------ CamoTest.php

  - Install the module (phpamo) following the standard process.


CONFIGURATION
------------

  - Configure user permissions in Administration » People » Permissions:
    'administer phpamo' to allow users to access to the module configuration
    settings.

  - Configure the module (admin/config/content/phpamo) with your Camo settings:

    * Camo key: the shared key used to generate the HMAC digest.
    * Camo domain: the server proxy domain.
    * URL format: between the accepted formats (query parameter or hex
      encoded).

  - Enable the text filter "Convert insecure images to secure Camo images." as
    part of the text format where external insecure image can be inserted
    (admin/config/content/formats/<text-format>).

SOURCES
-------
 - Camo:
   https://github.com/atmos/camo

 - Mixed-content:
   https://developer.mozilla.org/en-US/docs/Web/Security/Mixed_content

 - Github solution:
   https://github.com/blog/743-sidejack-prevention-phase-3-ssl-proxied-assets
