<?php

/**
 * @file
 * Admin callbacks for phpamo module.
 */

/**
 * Configuration settings form.
 */
function phpamo_admin() {
  $library = libraries_detect('phpamo');
  $error_message = isset($library['error message']) ? $library['error message'] : '';
  if (!empty($error_message)) {
    drupal_set_message(t('!error', array('!error' => $error_message)), 'error');
  }

  // Camo key field.
  $form['phpamo_camo_key'] = array(
    '#type' => 'textfield',
    '#title' => t('CAMO key'),
    '#required' => TRUE,
    '#default_value' => variable_get('phpamo_camo_key', ''),
    '#description' => t('The shared key used to generate the HMAC digest.'),
  );

  // Camo domain field.
  $form['phpamo_camo_domain'] = array(
    '#type' => 'textfield',
    '#title' => t('CAMO domain'),
    '#required' => TRUE,
    '#default_value' => variable_get('phpamo_camo_domain', ''),
    '#description' => t('The server proxy domain.'),
  );

  // Url format field.
  $help_text = t('CAMO supports two URL formats:');
  $help_text .= "<br>&nbsp;" . t("- Query parameter. E.g. 'http://example.org/&lt;digest&gt;?url=&lt;image-url&gt;'");
  $help_text .= "<br>&nbsp;" . t("- Hex encoded. E.g. 'http://example.org/&lt;digest&gt;/&lt;image-url&gt;'");
  $form['phpamo_camo_url_format'] = array(
    '#type' => 'select',
    '#title' => t('URL format'),
    '#options' => array(
      'query' => t('Query parameter'),
      'hex' => t('Hex encoded'),
    ),
    '#required' => TRUE,
    '#default_value' => variable_get('phpamo_camo_url_format', 'query'),
    '#description' => $help_text,
  );

  // Save form values in variables.
  return system_settings_form($form);
}
