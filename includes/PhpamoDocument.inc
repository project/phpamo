<?php

/**
 * @file
 * Used to detect insecure elements (request to http urls) in HTML.
 *
 * Original idea:
 * https://github.com/bramus/mixed-content-scan/blob/master/src/DOMDocument.php.
 */

/**
 * Class PhpamoDocument.
 *
 * Extend the DOMDocument class to make easier to find insecure assets.
 */
class PhpamoDocument extends \DOMDocument {

  /**
   * Collection that indicate sources of element attribute values we want check.
   *
   * @var array
   */
  protected $tags = array(
    'img' => array('src'),
  );

  /**
   * Extract URLs which are found to be insecure content.
   *
   * @return array
   *   URLs
   */
  public function extractMixedContentUrls() {
    $mixed_elements = array();

    // Find references to mixed content.
    foreach ($this->tags as $tag => $attributes) {
      /** @var \DOMElement $el */
      foreach ($this->getElementsByTagName($tag) as $el) {
        /** @var array $attributes */
        foreach ($attributes as $attribute) {
          if ($el->hasAttribute($attribute)) {
            $url = $el->getAttribute($attribute);
            if (stripos($url, 'http://') !== FALSE) {
              $mixed_elements[] = $url;
            }
          }
        }
      }
    }
    // Return found insecure urls.
    return $mixed_elements;
  }

}
