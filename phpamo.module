<?php

/**
 * @file
 * Phpamo module.
 */

use WillWashburn\Phpamo\Phpamo;
use WillWashburn\Phpamo\Formatter\QueryStringFormatter;
use WillWashburn\Phpamo\Encoder\QueryStringEncoder;

/**
 * Implements hook_help().
 */
function phpamo_help($path, $arg) {
  switch ($path) {
    case 'admin/help#phpamo':

      $filepath = dirname(__FILE__) . '/README.txt';
      if (file_exists($filepath)) {
        $readme = file_get_contents($filepath);
      }

      if (!isset($readme)) {
        return NULL;
      }
      $output = '<pre>' . $readme . '</pre>';

      return $output;
  }
}

/**
 * Implements hook_permission().
 */
function phpamo_permission() {
  return array(
    'administer phpamo' => array(
      'title' => t('Administer Phpamo.'),
      'description' => t('It allows users to access to the module administration page.'),
    ),
  );
}

/**
 * Implements hook_menu().
 */
function phpamo_menu() {
  $items = array();

  $items['admin/config/content/phpamo'] = array(
    'title' => 'Phpamo admin',
    'description' => 'Phpamo settings',
    'file' => 'phpamo.admin.inc',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('phpamo_admin'),
    'access arguments' => array('administer phpamo'),
  );

  return $items;
}

/**
 * Implements hook_libraries_info().
 */
function phpamo_libraries_info() {
  $libraries['phpamo'] = array(
    'name' => 'phpamo',
    'vendor url' => 'https://github.com/willwashburn/phpamo',
    'download url' => 'https://github.com/willwashburn/phpamo/archive/master.zip',
    'version arguments' => array(
      'file' => 'composer.json',
      'pattern' => '/"version": "([0-9a-zA-Z.-]+)"/',
    ),
    'files' => array(
      'php' => array(
        'src/Phpamo.php',
        'src/Encoder/EncoderInterface.php',
        'src/Encoder/HexEncoder.php',
        'src/Encoder/QueryStringEncoder.php',
        'src/Formatter/FormatterInterface.php',
        'src/Formatter/HexFormatter.php',
        'src/Formatter/QueryStringFormatter.php',
      ),
    ),
  );

  return $libraries;
}

/**
 * Instantiate the main library class - Phpamo with the saved configuration.
 *
 * @return mixed
 *   The instantiated Phpamo object or FALSE in case there is an error.
 */
function _phpamo_init_instance() {
  // Try to get the phpamo instance from static cache.
  $phpamo = &drupal_static('phpamo_instance');
  if (!empty($phpamo)) {
    return $phpamo;
  }

  $camo_conf = array(
    'phpamo_camo_key' => '',
    'phpamo_camo_domain' => '',
    'phpamo_camo_url_format' => '',
  );
  // Check all the configuration parameters are set.
  foreach ($camo_conf as $conf_key => $conf_value) {
    $camo_conf[$conf_key] = variable_get($conf_key);

    if ($camo_conf[$conf_key] == NULL) {
      watchdog('Phpamo', "Some configuration parameters can't be found. Review: !admin_page", array('!admin_page' => 'admin/config/content/phpamo'), WATCHDOG_ERROR);
      return FALSE;
    }
  }

  // Check the phpamo library is correctly installed.
  $library = libraries_detect('phpamo');
  $error_message = isset($library['error message']) ? $library['error message'] : '';
  if (!empty($error_message)) {
    watchdog('Phpamo', $error_message, array(), WATCHDOG_ERROR);
    return FALSE;
  }

  // Load the library.
  libraries_load('phpamo');

  // Instantiate the main library class.
  $formatter = ($camo_conf['phpamo_camo_url_format'] == 'query') ? new QueryStringFormatter(new QueryStringEncoder()) : NULL;
  $phpamo = new Phpamo(
    $camo_conf['phpamo_camo_key'],
    $camo_conf['phpamo_camo_domain'],
    $formatter
  );

  return $phpamo;
}

/**
 * Implements hook_filter_info().
 */
function phpamo_filter_info() {
  $filters['phpamo_insecure_images'] = array(
    'title' => t('Convert insecure images to secure Camo images.'),
    'description' => t('Convert insecure image sources to be used with Camo, using https.'),
    'process callback' => 'phpamo_insecure_images_filter',
  );

  return $filters;
}

/**
 * Filter process callback.
 *
 * Replace insecure image sources by urls understandable by CAMO.
 *
 * @param string $text
 *   The text with potential insecure assets.
 *
 * @return string
 *   Filtered text.
 */
function phpamo_insecure_images_filter($text) {
  if (!empty($text)) {

    // Create new PhpamoDocument.
    $doc = new PhpamoDocument();

    // Load up the HTML.
    if ($doc->loadHTML($text)) {

      // Extract insecure asset urls.
      $insecure_urls = $doc->extractMixedContentUrls();

      // Replace them by CAMO urls.
      if (!empty($insecure_urls)) {
        if ($phpamo = _phpamo_init_instance()) {
          foreach ($insecure_urls as $insecure_url) {

            $camo_url = $phpamo->camoHttpOnly($insecure_url);
            $text = str_replace($insecure_url, $camo_url, $text);
          }
        }
      }
    }
  }

  return $text;
}
